package cicd;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;

import static org.testng.Assert.assertTrue;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class spring_boot_selenium {
	WebDriver driver;
	static ExtentHtmlReporter htmlReporter;
	static protected ExtentReports extent;
	static protected ExtentTest extentlogger;
	
	@Test
	public void testPageContent() throws InterruptedException {
		extentlogger = extent.createTest("Spring boot verify page content");
		
		//driver.get("http://192.168.227.130:8080");
		driver.get("http://localhost:8080");
		Thread.sleep(500);
		String html = driver.getPageSource();
		if(html.contains("Greetings from Spring Boot!")) {
			extentlogger.log(Status.PASS, MarkupHelper.createLabel("verify page content passed", ExtentColor.GREEN));
		} else {
			extentlogger.log(Status.FAIL, MarkupHelper.createLabel("verify page content failed", ExtentColor.RED));
		}
		
	}
	
	@BeforeTest
	public void beforeTest() {
		
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/ExtentReport.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        extent.setSystemInfo("OS", "Mac Sierra");
        extent.setSystemInfo("Host Name", "Testing");
        extent.setSystemInfo("Environment", "QA");
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("Jasper Report Testing Result");
        htmlReporter.config().setReportName("Jasper Report test");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.DARK);
        
        
		WebDriverManager.chromedriver().setup();
		
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
  

	@AfterTest
	public void afterTest() {
		driver.quit();
		extent.flush();
	}

}
